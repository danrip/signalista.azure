﻿using System;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;

namespace Signalista.Azure.Subscriber.Two
{
    class Program
    {
        static void Main(string[] args)
        {
            var topicName = "headlines";
            var subscriptionName = "newsoutlet";

            var connectionString = CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.ConnectionString");
            var subscriptionClient = SubscriptionClient.CreateFromConnectionString(connectionString, topicName, subscriptionName);

            Console.WriteLine("Subscriber #2, waiting for stuff...");
                
            while (true)
            {
                var message = subscriptionClient.Receive();
                if (message != null)
                {
                    Console.WriteLine("(2) Headline received : {0} ", message.GetBody<string>());
                }
            }
        }
    }
}
