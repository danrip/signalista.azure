This is really simple project that demonstrates how to send receive messages using an Azure Service bus, a Queue and a Topic.

There are three different console applications. A sender, that publishes messages to the queue, and two subscribers. I added the second subscriber to see how subscriptions worked, and I learned that to have multiple listeners on the same queue (or Topic in this case) you need to define a subscription for each listener. 

I created this project mostly as a simple test to see how long it would take to get started, and I must say it was easier than I thought.

To get the sample running you'll need to set up a service bus, a queue and a topic in your Azure management portal. Also, you're going to need the connectionstring for the different App.configs.