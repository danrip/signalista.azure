﻿using System;
using System.Globalization;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;

namespace Signalista.Azure.Sender
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.ConnectionString");
            var topicName = "headlines";

            var topicClient = TopicClient.CreateFromConnectionString(connectionString, topicName);

            while (true)
            {
                Console.WriteLine("Press enter to publish a message");
                Console.ReadLine();
                
                var message = new BrokeredMessage("This is a headline");
                message.Properties["Sent"] = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                
                topicClient.Send(message);

                Console.WriteLine("Headline published");
            }
        }

        private static void ConfigureTopics()
        {
            
        }

        private static void DeleteTopics()
        {
            
        }
    }
}